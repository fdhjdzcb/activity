package com.example.businesscard


import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Check
import androidx.compose.material.icons.rounded.Email
import androidx.compose.material.icons.rounded.Phone
import androidx.compose.material.icons.rounded.Send
import androidx.compose.material3.Icon
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.businesscard.ui.theme.BusinessCardTheme


class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            BusinessCardTheme {
                Surface(
                    color = Color(0xFFCADF64),
                    modifier = Modifier.fillMaxSize()

                ) {
                    BusinessCard()
                }
            }
        }
        Log.d(getString(R.string.tag_log), "Logging onCreate")

        finish()
    }

    override fun onStart() {
        super.onStart()
        Log.d(getString(R.string.tag_log), "Logging onStart")
    }

    override fun onResume() {
        super.onResume()
        Log.d(getString(R.string.tag_log), "Logging onResume")
    }

    override fun onPause() {
        super.onPause()
        Log.d(getString(R.string.tag_log), "Logging onPause")
    }


    override fun onStop() {
        super.onStop()
        Log.d(getString(R.string.tag_log), "Logging onStop")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(getString(R.string.tag_log), "Logging onDestroy")
    }

    override fun onRestart() {
        super.onRestart()
        Log.d(getString(R.string.tag_log), "Logging onRestart")
    }
}

@Composable
fun BusinessCard() {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        val image = painterResource(R.drawable.icon)
        Image(
            painter = image,
            contentDescription = null,
            Modifier.fillMaxHeight(0.2f),

            )
        Text(
            text = stringResource(R.string.full_name),
            fontSize = 45.sp,
            fontStyle = FontStyle.Italic,
            color = Color.Black,
        )
        Text(
            text = stringResource(R.string.title),
            fontSize = 40.sp,
            color = Color(0xFF444444)
        )
        Spacer(
            modifier = Modifier.padding(bottom = 80.dp)
        )
        ContactRow(
            text = stringResource(R.string.phone_number),
            icon = Icons.Rounded.Phone
        )
        ContactRow(
            text = stringResource(R.string.telegram),
            icon = Icons.Rounded.Send
        )
        ContactRow(
            text = stringResource(R.string.vk),
            icon = Icons.Rounded.Check
        )
        ContactRow(
            text = stringResource(R.string.email),
            icon = Icons.Rounded.Email
        )
    }
}

@Composable
fun ContactRow(
    text: String, icon: ImageVector
) {
    Row(
        horizontalArrangement = Arrangement.Center,
        modifier = Modifier.padding(20.dp)
    ) {
        Icon(
            imageVector = icon,
            contentDescription = null,
            tint = Color(0xFF000000),
            modifier = Modifier.weight(1f)
        )
        Text(
            text = text,
            color = Color.Black,
            fontSize = 25.sp,
            modifier = Modifier.weight(3f)


        )
    }
}

@Preview(showBackground = true, showSystemUi = true)
@Composable
fun DefaultPreview() {
    Surface(
        modifier = Modifier.fillMaxSize(), color = Color(0xFFFFFFFF)
    ) {
        BusinessCardTheme {
            BusinessCard()
        }
    }
}