Первый запуск приложения:  
2024-02-28 01:44:26.403 10060-10060 MyLogger                com.example.businesscard             D  Logging onCreate  
2024-02-28 01:44:26.407 10060-10060 MyLogger                com.example.businesscard             D  Logging onStart  
2024-02-28 01:44:26.409 10060-10060 MyLogger                com.example.businesscard             D  Logging onResume  
Сворачивание приложения:
2024-02-28 01:44:42.417 10060-10060 MyLogger                com.example.businesscard             D  Logging onPause  
2024-02-28 01:44:42.441 10060-10060 MyLogger                com.example.businesscard             D  Logging onStop  
Открытие свернутого приложения:
2024-02-28 01:44:45.684 10060-10060 MyLogger                com.example.businesscard             D  Logging onRestart  
2024-02-28 01:44:45.684 10060-10060 MyLogger                com.example.businesscard             D  Logging onStart  
2024-02-28 01:44:45.685 10060-10060 MyLogger                com.example.businesscard             D  Logging onResume  
Поворот в горизонтальное положение (судя по логам UI дестроится и запускается заново):  
2024-02-28 01:45:00.242 10060-10060 MyLogger                com.example.businesscard             D  Logging onPause  
2024-02-28 01:45:00.246 10060-10060 MyLogger                com.example.businesscard             D  Logging onStop  
2024-02-28 01:45:00.257 10060-10060 MyLogger                com.example.businesscard             D  Logging onDestroy  
2024-02-28 01:45:00.283 10060-10060 MyLogger                com.example.businesscard             D  Logging onCreate  
2024-02-28 01:45:00.285 10060-10060 MyLogger                com.example.businesscard             D  Logging onStart  
2024-02-28 01:45:00.286 10060-10060 MyLogger                com.example.businesscard             D  Logging onResume  
Поворот обратно в вертикальное положение  
2024-02-28 01:45:04.902 10060-10060 MyLogger                com.example.businesscard             D  Logging onPause
2024-02-28 01:45:04.905 10060-10060 MyLogger                com.example.businesscard             D  Logging onStop
2024-02-28 01:45:04.911 10060-10060 MyLogger                com.example.businesscard             D  Logging onDestroy
2024-02-28 01:45:04.932 10060-10060 MyLogger                com.example.businesscard             D  Logging onCreate
2024-02-28 01:45:04.934 10060-10060 MyLogger                com.example.businesscard             D  Logging onStart
2024-02-28 01:45:04.935 10060-10060 MyLogger                com.example.businesscard             D  Logging onResume
Полный выход из приложения
2024-02-28 01:45:09.513 10060-10060 MyLogger                com.example.businesscard             D  Logging onPause
2024-02-28 01:45:09.519 10060-10060 MyLogger                com.example.businesscard             D  Logging onStop
2024-02-28 01:45:09.526 10060-10060 MyLogger                com.example.businesscard             D  Logging onDestroy
---------------------------- PROCESS ENDED (10060) for package com.example.businesscard ----------------------------


После добавления finish():
2024-02-28 01:48:46.355 10234-10234 MyLogger                com.example.businesscard             D  Logging onCreate
2024-02-28 01:48:47.030 10234-10234 MyLogger                com.example.businesscard             D  Logging onDestroy
Создается и тут же завершается (приложение вылетает)
